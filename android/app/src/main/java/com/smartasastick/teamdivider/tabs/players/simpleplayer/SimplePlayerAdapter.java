package com.smartasastick.teamdivider.tabs.players.simpleplayer;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartasastick.teamdivider.R;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.tabs.players.PlayerChangeSelectionListener;

import java.util.ArrayList;
import java.util.List;

class SimplePlayerAdapter extends ArrayAdapter<Player> {

    private static String TAG = SimplePlayerAdapter.class.getCanonicalName();
    private List<PlayerChangeSelectionListener> mListeners;
    private Context context;
    private List<Player> players;

    SimplePlayerAdapter(Context context, int resource, List<Player> players, Fragment parentFragment) {
        super(context, resource, players);
        this.context = context;
        this.players = players;

        try {
            mListeners = new ArrayList<>();
            mListeners.add((PlayerChangeSelectionListener) context);
            mListeners.add((PlayerChangeSelectionListener)parentFragment);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement PlayerChangeSelectionListener" +
            context.getClass().getCanonicalName());
        }
    }
    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final Player player = players.get(position);

        View view = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.player_simple_list_item, parent, false);

            holder = new ViewHolder();
            holder.selectedCheckBox = (CheckBox) view.findViewById(R.id.checkBox);
            holder.deleteImage = (ImageView) view.findViewById(R.id.deleteSimplePlayer);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.selectedCheckBox.setChecked(player.getSelected());
        holder.selectedCheckBox.setText(player.getName());
        holder.selectedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                final boolean isChecked = holder.selectedCheckBox.isChecked();
                player.setSelected(isChecked);

                for (PlayerChangeSelectionListener mListener : mListeners) {
                    mListener.playerChangedSelection(player);
                }
            }
        });

        holder.deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Deleting player");

                for (PlayerChangeSelectionListener mListener : mListeners) {
                    mListener.deletePlayer(player);
                }
            }
        });

        return view;
    }

    private class ViewHolder {
        TextView playerName;
        CheckBox selectedCheckBox;
        ImageView deleteImage;
    }
}