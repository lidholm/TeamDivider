package com.smartasastick.teamdivider.tabs.teams;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.smartasastick.teamdivider.R;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.model.Team;
import com.smartasastick.teamdivider.tabs.players.levelplayer.PlayerLevelComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class TeamsFragment extends Fragment {
    static Random rand;
    private static String TAG = TeamsFragment.class.getCanonicalName();
    private static DatabaseReference databaseReference;
    private static boolean useLevels;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private List<Player> players = null;
    private TeamsAdapter adapter;
    private List<Team> teams;

    public static TeamsFragment newInstance(DatabaseReference databaseReference, boolean useLevels) {
        Log.i(TAG, "in newInstance");
        TeamsFragment.databaseReference = databaseReference;
        TeamsFragment.rand = new Random();
        TeamsFragment.useLevels = useLevels;
        Bundle args = new Bundle();
        TeamsFragment fragment = new TeamsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        teams = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        setupSelectedPlayerListener();

        View view = inflater.inflate(R.layout.teams_fragment, container, false);

        final Spinner numberOfTeamsSpinner = (Spinner) view.findViewById(R.id.numberOfTeamsSpinner);
        Button shuffleButton = (Button) view.findViewById(R.id.shuffleTeamsButton);

        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int numberOfTeams = Integer.parseInt(numberOfTeamsSpinner.getSelectedItem().toString());
                setNewTeams(numberOfTeams);
            }
        });

        ListView teamsList = (ListView) view.findViewById(R.id.teamsList);

        populateTeamsList(teamsList);

        return view;
    }

    private void setNewTeams(int numberOfTeams) {
        List<Player> selectedPlayers = new ArrayList<>();
        for (Player player: players) {
            if (player.getSelected()) {
                selectedPlayers.add(player);
            }
        }
        teams.clear();
        teams.addAll(shuffleTeams(numberOfTeams, selectedPlayers));
        adapter.notifyDataSetChanged();

        Toast.makeText(getContext(), "shuffle for " + numberOfTeams + " teams", Toast.LENGTH_SHORT).show();
    }

    List<Team> shuffleTeams(int numberOfTeams, List<Player> selectedPlayers) {
        Log.i(TAG, "shuffle teams with useLevels: " + useLevels);
        if (useLevels) {
            return shuffleTeamsWithLevels(numberOfTeams, selectedPlayers);
        } else {
            return shuffleTeamsWithoutLevels(numberOfTeams, selectedPlayers);
        }
    }

    List<Team> shuffleTeamsWithoutLevels(int numberOfTeams, List<Player> selectedPlayers) {
        List<Player> players = new ArrayList<>(selectedPlayers);
        List<Team> teams = new ArrayList<>();
        for (int i=0; i<numberOfTeams; i++) {
            teams.add(new Team());
        }

        int teamNum = 0;
        while (players.size() > 0) {
            int r = rand.nextInt((players.size()));
            Player player = players.remove(r);
            teams.get(teamNum).addPlayer(player);

            teamNum++;
            if (teamNum >= numberOfTeams) {
                teamNum = 0;
            }
        }
        return teams;
    }

    List<Team> shuffleTeamsWithLevels(int numberOfTeams, List<Player> selectedPlayers) {
        List<Player> players = new ArrayList<>(selectedPlayers);
        List<Team> teamsThatNeedPlayer = new ArrayList<>();
        for (int i = 0; i < numberOfTeams; i++) {
            teamsThatNeedPlayer.add(new Team());
        }

        List<Team> teamsThatGotPlayer = new ArrayList<>();
        Collections.shuffle(players);
        Collections.sort(players, new PlayerLevelComparator());

        while (players.size() > 0) {
            int r = rand.nextInt(teamsThatNeedPlayer.size());
            Team team = teamsThatNeedPlayer.remove(r);
            Player player = players.remove(0);
            team.addPlayer(player);

            teamsThatGotPlayer.add(team);

            if (teamsThatNeedPlayer.size() == 0) {
                teamsThatNeedPlayer.addAll(teamsThatGotPlayer);
                teamsThatGotPlayer.clear();
            }
        }

        List<Team> teams = new ArrayList<>();
        teams.addAll(teamsThatGotPlayer);
        teams.addAll(teamsThatNeedPlayer);
        return teams;
    }

    private void populateTeamsList(ListView teamsList) {
        adapter = new TeamsAdapter(getContext(), teams);
        teamsList.setAdapter(adapter);
    }

    private void clearTeams() {
        if (listDataHeader == null) {
            listDataHeader = new ArrayList<>();
            listDataChild = new HashMap<>();
        }
        Log.i(TAG, "clearing teams");
        listDataHeader.clear();
        listDataChild.clear();
        adapter.notifyDataSetChanged();
    }

    private void setupSelectedPlayerListener() {
        if (players == null) {
            players = new ArrayList<>();
        }

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                players.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    String key = postSnapshot.getKey();
                    Player player = postSnapshot.getValue(Player.class);
                    player.setKey(key);
                    players.add(player);
                }

                clearTeams();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void setUseLevels(boolean useLevels) {
        TeamsFragment.useLevels = useLevels;
    }
}