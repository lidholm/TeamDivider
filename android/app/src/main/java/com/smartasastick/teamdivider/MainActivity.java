/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * For more information on setting up and running this sample code, see
 * https://firebase.google.com/docs/invites/android
 */
package com.smartasastick.teamdivider;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.signin.GoogleSignInActivity;
import com.smartasastick.teamdivider.tabs.TabFragmentPagerAdapter;
import com.smartasastick.teamdivider.tabs.players.PlayerChangeSelectionListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener,
        PlayerChangeSelectionListener {

    public static final String ANONYMOUS = "anonymous";
    public static final int ADD_PLAYER = 6534;
    private static final String TAG = "MainActivity";
    private GoogleApiClient mGoogleApiClient;
    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUid;
    private DatabaseReference databaseReference;
    private ViewPager viewPager;
    private TabFragmentPagerAdapter tabFragmentPagerAdapter;
    private boolean useLevels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        Toolbar toolbar = setupToolbar();

        setupNewPlayerButton();

        if (!setupLogin()) return;

        setupDatabase();

        setupDrawer(toolbar);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabFragmentPagerAdapter = new TabFragmentPagerAdapter(getSupportFragmentManager(), MainActivity.this, databaseReference);
        viewPager.setAdapter(tabFragmentPagerAdapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.show();
                        break;
                    case 1:
                        fab.hide();
                        break;
                    default:
                        fab.hide();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });
    }

    private void setupNewPlayerButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddPlayerActivity.class);
                intent.putExtra(AddPlayerActivity.ADD_PLAYER, true);
                startActivityForResult(intent, ADD_PLAYER);
            }
        });
    }

    private Toolbar setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        return toolbar;
    }

    private boolean setupLogin() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, GoogleSignInActivity.class));
            finish();
            return false;
        } else {
            if (mFirebaseUser.getPhotoUrl() != null) {
                mUid = mFirebaseUser.getUid();
            }
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        return true;
    }

    private void setupDrawer(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Log.i(TAG, "mUid is: " + mUid);
        String selectedGroup = "Duckburg";
        String node = "users/" + mUid + "/" + selectedGroup;
        Log.i(TAG, "node is: " + node);
        databaseReference = database.getReference(node);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.i(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.navigation, menu);
        MenuItem useLevelsItem = menu.getItem(1);
        useLevelsItem.setChecked(useLevels);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            //noinspection SimplifiableIfStatement
            case R.id.action_settings:
                return true;
            case R.id.toggle_use_levels:
                useLevels = !item.isChecked();
                item.setChecked(useLevels);
                Log.i(TAG, "useLevels: " + useLevels);
                tabFragmentPagerAdapter.setUseLevels(useLevels);
                int currentItem = viewPager.getCurrentItem();
                viewPager.setCurrentItem(currentItem);
                tabFragmentPagerAdapter.notifyDataSetChanged();
                Log.i(TAG, "clicked the checkbox");
                return true;
            case R.id.action_sign_out:
                signOut();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void signOut() {
        mFirebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        mFirebaseUser = null;
        startActivity(new Intent(this, GoogleSignInActivity.class));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_PLAYER) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                int level = data.getIntExtra("level", 1);

                Log.i(TAG, "name:: " + name);
                Log.i(TAG, "level:: " + level);

                AddPlayer(name, level);
            }
        }
    }

    private void AddPlayer(String name, int level) {
        Player player = new Player(name, level, false);
        databaseReference.push().setValue(player);
        //TODO: Set the key to the player too, and remove that from SimplePlayerListFragment
    }

    @Override
    public void playerChangedSelection(Player player) {
        DatabaseReference childRef = databaseReference.child(player.getKey());
        childRef.setValue(player);
    }

    @Override
    public void deletePlayer(Player player) {
        DatabaseReference childRef = databaseReference.child(player.getKey());
        childRef.removeValue();
    }
}
