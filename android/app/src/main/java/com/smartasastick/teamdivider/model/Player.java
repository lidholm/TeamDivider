package com.smartasastick.teamdivider.model;

import com.google.firebase.database.Exclude;

public class Player {
    private String name;
    private int level;
    private boolean selected;
    @Exclude
    private String key;

    public Player() { }

    public Player(String name, int level, boolean selected) {
        this.name = name;
        this.level = level;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    public String toString() {
        return "<Player: " + name + ", " + level + ", " + selected + ">";
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
