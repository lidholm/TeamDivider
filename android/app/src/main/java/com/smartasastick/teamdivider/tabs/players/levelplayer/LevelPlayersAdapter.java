package com.smartasastick.teamdivider.tabs.players.levelplayer;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.smartasastick.teamdivider.R;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.tabs.players.PlayerChangeSelectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class LevelPlayersAdapter extends BaseAdapter {
    private final String TAG = LevelPlayersAdapter.class.getCanonicalName();
    private final Context _context;
    private final HashMap<Integer, List<Player>> players;
    private final LayoutInflater inflater;
    private List<PlayerChangeSelectionListener> mListeners;
    private List<PlayersListItem> playersList;

    LevelPlayersAdapter(Context context, HashMap<Integer, List<Player>> players, Fragment parentFragment) {
        Log.d(TAG, "LevelPlayersAdapter");

        this._context = context;
        this.players = players;

        playersList = new ArrayList<>();

        inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        try {
            mListeners = new ArrayList<>();
            mListeners.add((PlayerChangeSelectionListener) context);
            mListeners.add((PlayerChangeSelectionListener) parentFragment);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement PlayerChangeSelectionListener" + context.getClass().getCanonicalName());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return playersList.get(position).getType().getValue();
    }

    @Override
    public int getViewTypeCount() {
        Log.d(TAG, "getViewTypeCount");
        return PlayersListItem.PlayersListItemType.values().length;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount(): playersList.size(): " + playersList.size());
        return playersList.size();
    }

    @Override
    public Object getItem(int i) {
        Log.d(TAG, "getItem(" + i + ")");
        return playersList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Log.d(TAG, "getView");

        final Player player = playersList.get(position).getPlayer();

        LevelPlayersAdapter.ViewHolder holder;
        int type = getItemViewType(position);
        Log.d(TAG, "getView " + position + " " + convertView + " type = " + type);

        if (convertView == null) {
            holder = new LevelPlayersAdapter.ViewHolder();
            if (type == PlayersListItem.PlayersListItemType.HEADER.getValue()) {
                convertView = inflater.inflate(R.layout.player_level_list_header_item, null);
                holder.textView = (TextView) convertView.findViewById(R.id.levelPlayersListHeader);
            } else {
                convertView = inflater.inflate(R.layout.player_level_list_player_item, null);
                holder.textView = (TextView) convertView.findViewById(R.id.levelPlayersListItem);

                final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBoxLevelPlayer);
                checkBox.setChecked(player.getSelected());
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        final boolean isChecked = checkBox.isChecked();
                        player.setSelected(isChecked);
                        for (PlayerChangeSelectionListener mListener : mListeners) {
                            mListener.playerChangedSelection(player);
                        }
                    }
                });
            }
            convertView.setTag(holder);
        } else {
            holder = (LevelPlayersAdapter.ViewHolder) convertView.getTag();
        }
        if (type == PlayersListItem.PlayersListItemType.HEADER.getValue()) {
            holder.textView.setText(playersList.get(position).getHeaderText());
        } else {
            holder.textView.setText(playersList.get(position).getPlayer().getName());
        }
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        Log.i(TAG, "notifyDataSetChanged");
        playersList.clear();
        for (Integer key : players.keySet()) {
            playersList.add(new PlayersListItem(PlayersListItem.PlayersListItemType.HEADER, "Level " + key, null));
            for (Player player : players.get(key)) {
                playersList.add(new PlayersListItem(PlayersListItem.PlayersListItemType.PLAYER, null, player));
            }
        }
        super.notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView textView;
        public CheckBox checkBox;
    }
}
