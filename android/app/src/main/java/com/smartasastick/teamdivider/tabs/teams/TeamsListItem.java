package com.smartasastick.teamdivider.tabs.teams;


public class TeamsListItem {
    private TeamsListItemType type;
    private String text;

    public TeamsListItem(TeamsListItemType type, String text) {
        this.type = type;
        this.text = text;
    }

    public TeamsListItemType getType() {
        return type;
    }

    public void setType(TeamsListItemType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public enum TeamsListItemType {
        TEAM_HEADER(0),
        PLAYER(1);

        private final int id;

        TeamsListItemType(int id) {
            this.id = id;
        }

        public int getValue() {
            return id;
        }
    }

}
