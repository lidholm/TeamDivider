package com.smartasastick.teamdivider.tabs.players.levelplayer;


import com.smartasastick.teamdivider.model.Player;

public class PlayersListItem {
    private PlayersListItemType type;
    private String headerText;
    private Player player;

    public PlayersListItem(PlayersListItemType type, String headerText, Player player) {
        this.type = type;
        this.headerText = headerText;
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayersListItemType getType() {
        return type;
    }

    public void setType(PlayersListItemType type) {
        this.type = type;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public enum PlayersListItemType {
        HEADER(0),
        PLAYER(1);

        private final int id;

        PlayersListItemType(int id) {
            this.id = id;
        }

        public int getValue() {
            return id;
        }
    }

}
