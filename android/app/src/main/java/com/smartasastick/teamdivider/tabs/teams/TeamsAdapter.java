package com.smartasastick.teamdivider.tabs.teams;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartasastick.teamdivider.R;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.model.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamsAdapter extends BaseAdapter {
    private final String TAG = TeamsAdapter.class.getCanonicalName();
    private final Context _context;
    private final LayoutInflater inflater;
    private List<Team> teams;
    private List<TeamsListItem> teamsList;

    TeamsAdapter(Context context, List<Team> teams) {
        this._context = context;
        this.teams = teams;
        teamsList = new ArrayList<>();

        inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void notifyDataSetChanged() {
        createInternalList();
        super.notifyDataSetChanged();
    }

    private void createInternalList() {
        teamsList.clear();
        int i = 1;
        for (Team team : teams) {
            teamsList.add(new TeamsListItem(TeamsListItem.TeamsListItemType.TEAM_HEADER, "TEAM " + i));
            for (Player player : team.getPlayers()) {
                teamsList.add(new TeamsListItem(TeamsListItem.TeamsListItemType.PLAYER, player.getName()));
            }
            i++;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return teamsList.get(position).getType().getValue();
    }

    @Override
    public int getViewTypeCount() {
        return TeamsListItem.TeamsListItemType.values().length;
    }

    @Override
    public int getCount() {
        return teamsList.size();
    }

    @Override
    public Object getItem(int i) {
        return teamsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        int type = getItemViewType(position);
        System.out.println("getView " + position + " " + convertView + " type = " + type);
        if (convertView == null) {
            holder = new ViewHolder();
            if (type == TeamsListItem.TeamsListItemType.TEAM_HEADER.getValue()) {
                convertView = inflater.inflate(R.layout.teams_list_header_item, null);
                holder.textView = (TextView) convertView.findViewById(R.id.lblListHeader);
            } else {
                convertView = inflater.inflate(R.layout.teams_list_player_item, null);
                holder.textView = (TextView) convertView.findViewById(R.id.lblListItem);
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView.setText(teamsList.get(position).getText());
        return convertView;

    }

    public static class ViewHolder {
        public TextView textView;
    }
}
