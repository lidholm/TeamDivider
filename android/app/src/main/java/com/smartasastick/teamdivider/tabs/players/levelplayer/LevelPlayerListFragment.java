package com.smartasastick.teamdivider.tabs.players.levelplayer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.smartasastick.teamdivider.R;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.tabs.players.PlayerChangeSelectionListener;
import com.smartasastick.teamdivider.tabs.players.PlayerListFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LevelPlayerListFragment extends PlayerListFragment implements PlayerChangeSelectionListener {
    private static final String TAG = LevelPlayerListFragment.class.getCanonicalName();

    private LevelPlayersAdapter listAdapter;
    private HashMap<Integer, List<Player>> orderedPlayers;

    protected View createView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.player_levels_list_fragment, container, false);
        numberOfSelectedPlayersLabel = (TextView) view.findViewById(R.id.numberOfSelectedPlayersLevel);
        return view;
    }

    @Override
    protected void createListAdapter(View view) {
        orderedPlayers = new HashMap<>();

        ListView listView = (ListView) view.findViewById(R.id.levelPlayersList);
        listAdapter = new LevelPlayersAdapter(getContext(), orderedPlayers, this);
        listView.setAdapter(listAdapter);
    }

    @Override
    protected void notifyDataSetChanged() {
        orderedPlayers.clear();
        for(Player p : players) {
            Integer level = Integer.valueOf(p.getLevel());

            if (!orderedPlayers.containsKey(level)) {
                orderedPlayers.put(level, new ArrayList<Player>());
            }
            orderedPlayers.get(level).add(p);
        }

        listAdapter.notifyDataSetChanged();
    }

}