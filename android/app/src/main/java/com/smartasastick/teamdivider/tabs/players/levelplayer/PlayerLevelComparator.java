package com.smartasastick.teamdivider.tabs.players.levelplayer;

import com.smartasastick.teamdivider.model.Player;

import java.util.Comparator;

public class PlayerLevelComparator implements Comparator<Player> {
    @Override
    public int compare(Player playerA, Player playerB) {
        return (int) (playerA.getLevel() - playerB.getLevel());
    }
}