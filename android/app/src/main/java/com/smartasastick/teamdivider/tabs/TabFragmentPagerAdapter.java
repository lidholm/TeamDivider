package com.smartasastick.teamdivider.tabs;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.smartasastick.teamdivider.tabs.players.PlayerListFragment;
import com.smartasastick.teamdivider.tabs.players.levelplayer.LevelPlayerListFragment;
import com.smartasastick.teamdivider.tabs.players.simpleplayer.SimplePlayerListFragment;
import com.smartasastick.teamdivider.tabs.teams.TeamsFragment;

public class TabFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private String TAG = TabFragmentPagerAdapter.class.getCanonicalName();

    private String tabTitles[] = new String[] { "Players", "Teams" }; //TODO: Move to string resources

    private Context context;
    private DatabaseReference databaseReference;
    private boolean useLevels;
    private TeamsFragment teamsFragment;


    public TabFragmentPagerAdapter(FragmentManager fm, Context context, DatabaseReference databaseReference) {
        super(fm);
        this.context = context;
        this.databaseReference = databaseReference;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "get item " + position);
        Log.i(TAG, "useLevels " + useLevels);
        switch (position) {
            case 0:
                return PlayerListFragment.newInstance(this.databaseReference, useLevels);
            case 1:
                if (teamsFragment == null) {
                    teamsFragment = TeamsFragment.newInstance(this.databaseReference, useLevels);
                }
                return teamsFragment;
        }
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        Log.i(TAG, "getItemPosition");
        Log.i(TAG, object.getClass().getName());
        if (object.getClass().getName().equals(LevelPlayerListFragment.class.getName()) ||
                object.getClass().getName().equals(SimplePlayerListFragment.class.getName())) {
            Log.i(TAG, "POSITION_NONE");
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public void setUseLevels(boolean useLevels) {
        this.useLevels = useLevels;
        if (teamsFragment != null) {
            teamsFragment.setUseLevels(useLevels);
        }
    }
}
