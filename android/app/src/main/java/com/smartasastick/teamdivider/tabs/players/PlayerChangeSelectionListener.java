package com.smartasastick.teamdivider.tabs.players;

import com.smartasastick.teamdivider.model.Player;

public interface PlayerChangeSelectionListener {
    void playerChangedSelection(Player player);

    void deletePlayer(Player player);
}

