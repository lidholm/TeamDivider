package com.smartasastick.teamdivider.tabs.teams;

import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.model.Team;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TeamsFragmentTest {
    private TeamsFragment fragment;
    private List<Player> players;

    @Before
    public void setUp() throws Exception {
        fragment = new TeamsFragment();

        players = new ArrayList<>();
        players.add(new Player("Alice", 1, true));
        players.add(new Player("Bob", 2, true));
        players.add(new Player("Charlie", 2, true));
        players.add(new Player("David", 1, true));
        players.add(new Player("Eve", 3, true));
        players.add(new Player("Faythe", 3, true));
    }

    @Test
    public void shufflingWithoutLevelsGivesSameNumberOfPlayersPerTeam() {
        fragment.useLevels = false;
        List<Team> teams = fragment.shuffleTeams(2, players);
        assertThat(teams.size(), is(2));
        assertThat(teams.get(0).getPlayers().size(), is(3));
        assertThat(teams.get(1).getPlayers().size(), is(3));
    }

    @Test
    public void shufflingWithLevelsGivesWeightedTeams() {
        fragment.useLevels = true;
        TeamsFragment.rand = new Random();
        List<Team> teams = fragment.shuffleTeams(2, players);
        assertThat(teams.size(), is(2));
        assertThat(teams.get(0).getPlayers().size(), is(3));
        assertThat(teams.get(1).getPlayers().size(), is(3));

        for (int teamNumber = 0; teamNumber <= 1; teamNumber++) {
            for (int levelNumber = 1; levelNumber <= 3; levelNumber++) {
                assertThat(teamHasPlayerWithLevel(teams.get(teamNumber), levelNumber), is(true));
            }
        }
    }

    private boolean teamHasPlayerWithLevel(Team team, int playerLevel) {
        for (Player player : team.getPlayers()) {
            if (player.getLevel() == playerLevel) {
                return true;
            }
        }
        return false;
    }
}