package com.smartasastick.teamdivider;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;

public class AddPlayerActivity extends AppCompatActivity {
    public static final String ADD_PLAYER = "ADD_PLAYER";
    public static final String NAME = "name";
    public static final String LEVEL = "level";
    private String TAG = AddPlayerActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_player_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_player:
                String name = ((EditText)findViewById(R.id.addPlayerName)).getText().toString();
                int level = Integer.parseInt((String)((Spinner)findViewById(R.id.levelSpinner)).getSelectedItem());
                Log.i(TAG, "name: " + name);
                Log.i(TAG, "level: " + level);
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(AddPlayerActivity.NAME, name);
                intent.putExtra(AddPlayerActivity.LEVEL, level);
                setResult(RESULT_OK, intent);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
