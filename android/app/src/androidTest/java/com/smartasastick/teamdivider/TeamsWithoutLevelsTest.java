/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smartasastick.teamdivider;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;

import com.smartasastick.teamdivider.tabs.teams.TeamsFragment;
import com.smartasastick.teamdivider.tabs.teams.TeamsListItem;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

@RunWith(AndroidJUnit4.class)
public class TeamsWithoutLevelsTest {
    private static String TAG = TeamsFragment.class.getCanonicalName();
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);
    private String player1 = "Donald Duck";
    private String player2 = "Mickey Mouse";
    private String player3 = "Minnie Mouse";
    private String player4 = "Gyro Gearloose";
    private String player5 = "Daisy Duck";

    private static Matcher<View> checkAllPlayersInTeams(final List<String> expectedPlayers, final int expectedNumberOfTeams) {
        return new TypeSafeMatcher<View>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("!!for teams!!");
            }

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof AdapterView)) {
                    return false;
                }
                @SuppressWarnings("rawtypes")
                Adapter adapter = ((AdapterView) view).getAdapter();
                ArrayList<String> playersInList = new ArrayList<>();
                int numberOfTeams = 0;
                int[] numberPlayersPerTeams = new int[100];
                for (int i = 0; i < adapter.getCount(); i++) {
                    TeamsListItem listItem = (TeamsListItem) adapter.getItem(i);
                    if (listItem.getType() == TeamsListItem.TeamsListItemType.PLAYER) {
                        String playerName = listItem.getText();
                        playersInList.add(playerName);
                        numberPlayersPerTeams[numberOfTeams - 1]++;
                    }
                    if (listItem.getType() == TeamsListItem.TeamsListItemType.TEAM_HEADER) {
                        numberOfTeams++;
                    }
                }
                if (!(expectedPlayers.containsAll(playersInList) && playersInList.containsAll(expectedPlayers))) {
                    Log.e(TAG, "Some player is missing");
                    return false;
                }
                if (expectedNumberOfTeams != numberOfTeams) {
                    Log.e(TAG, "Wrong number of teams, " + numberOfTeams);
                    return false;
                }
                for (int i = 0; i < expectedNumberOfTeams - 1; i++) {
                    if (numberPlayersPerTeams[i] < numberPlayersPerTeams[i + 1] - 1) {
                        Log.e(TAG, "One team is having too many or too few players, " + i + ", " + numberPlayersPerTeams[i] + ", " + numberPlayersPerTeams[i + 1]);
                        return false;
                    }
                    if (numberPlayersPerTeams[i] > numberPlayersPerTeams[i + 1] + 1) {
                        Log.e(TAG, "One team is having too many or too few players, " + i + ", " + numberPlayersPerTeams[i] + ", " + numberPlayersPerTeams[i + 1]);
                        return false;
                    }
                }
                return true;
            }
        };
    }

    @Before
    public void setUp() {
        addPlayer(player1, 2);
        addPlayer(player2, 1);
        addPlayer(player3, 1);
        addPlayer(player4, 2);
        addPlayer(player5, 2);
    }

    @Test
    public void testTeamsWithoutLevels() throws InterruptedException {
        onData(anything()).inAdapterView(withId(R.id.players)).atPosition(0).onChildView(withId(R.id.checkBox)).check(matches(withText(player1)));
        onData(anything()).inAdapterView(withId(R.id.players)).atPosition(1).onChildView(withId(R.id.checkBox)).check(matches(withText(player2)));
        onData(anything()).inAdapterView(withId(R.id.players)).atPosition(2).onChildView(withId(R.id.checkBox)).check(matches(withText(player3)));
        onData(anything()).inAdapterView(withId(R.id.players)).atPosition(3).onChildView(withId(R.id.checkBox)).check(matches(withText(player4)));

        SelectAllPlayers();
        onData(anything()).inAdapterView(withId(R.id.players)).atPosition(3).onChildView(withId(R.id.checkBox)).perform(click());

        onView(allOf(withText("Teams"), isDescendantOfA(withId(R.id.sliding_tabs)))).perform(click());

        onView(withId(R.id.numberOfTeamsSpinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("2"))).perform(click());

        onView(withId(R.id.shuffleTeamsButton)).perform(click());

        SystemClock.sleep(1800); // Wait a little until the content is loaded

        List<String> selectedPlayers = Arrays.asList(player1, player2, player3, player5);

        onView(withId(R.id.teamsList)).check(matches(checkAllPlayersInTeams(selectedPlayers, 2)));

        onView(allOf(withText("Players"), isDescendantOfA(withId(R.id.sliding_tabs)))).perform(click());
        for (int i = 0; i < 5; i++) {
            onData(anything()).inAdapterView(withId(R.id.players)).atPosition(0).onChildView(withId(R.id.deleteSimplePlayer)).perform(click());
        }
    }

    private void addPlayer(String name, int level) {
        onView(withId(R.id.fab)).perform(click());

        onView(withId(R.id.addPlayerName)).perform(typeText(name), closeSoftKeyboard());

        onView(withId(R.id.levelSpinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is(level + ""))).perform(click());

        onView(withId(R.id.action_save_player)).perform(click());
    }

    private void SelectAllPlayers() {
        for (int i = 0; i < 5; i++) {
            onData(anything()).inAdapterView(withId(R.id.players)).atPosition(i).onChildView(withId(R.id.checkBox)).perform(click());
        }
    }

}
