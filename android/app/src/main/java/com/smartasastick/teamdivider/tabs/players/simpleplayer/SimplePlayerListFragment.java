package com.smartasastick.teamdivider.tabs.players.simpleplayer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.smartasastick.teamdivider.R;
import com.smartasastick.teamdivider.tabs.players.PlayerChangeSelectionListener;
import com.smartasastick.teamdivider.tabs.players.PlayerListFragment;

public class SimplePlayerListFragment extends PlayerListFragment implements PlayerChangeSelectionListener {
    private static final String TAG = SimplePlayerListFragment.class.getCanonicalName();
    private SimplePlayerAdapter listAdapter;

    protected View createView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.player_simple_list_fragment, container, false);

        numberOfSelectedPlayersLabel = (TextView) view.findViewById(R.id.numberOfSelectedPlayersSimple);

        return view;
    }

    protected void createListAdapter(View view) {
        ListView listView = (ListView) view.findViewById(R.id.players);

        listAdapter = new SimplePlayerAdapter(this.getContext(), R.layout.player_simple_list_item, players, this);
        listView.setAdapter(listAdapter);
    }

    @Override
    protected void notifyDataSetChanged() {
        listAdapter.notifyDataSetChanged();
    }

}