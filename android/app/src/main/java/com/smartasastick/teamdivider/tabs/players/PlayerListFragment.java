package com.smartasastick.teamdivider.tabs.players;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.smartasastick.teamdivider.model.Player;
import com.smartasastick.teamdivider.tabs.players.levelplayer.LevelPlayerListFragment;
import com.smartasastick.teamdivider.tabs.players.simpleplayer.SimplePlayerListFragment;

import java.util.ArrayList;
import java.util.List;

public abstract class PlayerListFragment extends Fragment implements PlayerChangeSelectionListener {
    private static final String TAG = PlayerListFragment.class.getCanonicalName();
    protected static DatabaseReference databaseReference;
    protected TextView numberOfSelectedPlayersLabel;
    protected List<Player> players;

    public static PlayerListFragment newInstance(DatabaseReference databaseReference, boolean useLevels) {
        Log.i(TAG, "useLevels: " + useLevels);
        PlayerListFragment.databaseReference = databaseReference;
        Bundle args = new Bundle();

        PlayerListFragment fragment;
        if (useLevels) {
            fragment = new LevelPlayerListFragment();
        } else {
            fragment = new SimplePlayerListFragment();
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");

        View view = createView(inflater, container);

        players = new ArrayList<>();

        createListAdapter(view);

        setUpDatabaseListener();

        this.setHasOptionsMenu(true);

        return view;
    }

    protected abstract View createView(LayoutInflater inflater, ViewGroup container);

    protected abstract void createListAdapter(View view);

    private void setUpDatabaseListener() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                players.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    String key = postSnapshot.getKey();
                    Player player = postSnapshot.getValue(Player.class);
                    player.setKey(key);
                    players.add(player);
                }
                notifyDataSetChanged();

                setNumberOfSelectedPlayersLabel();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    protected abstract void notifyDataSetChanged();

    @Override
    public void playerChangedSelection(Player p) {
        setNumberOfSelectedPlayersLabel();
    }

    protected void setNumberOfSelectedPlayersLabel() {
        if (players == null) {
            return;
        }

        int totalNumberOfPlayers = players.size();

        int numberOfSelectedPlayers = 0;
        for (Player player : players) {
            if (player.getSelected()) {
                numberOfSelectedPlayers++;
            }
        }

        numberOfSelectedPlayersLabel.setText(numberOfSelectedPlayers + "/" + totalNumberOfPlayers + " are selected");
    }

    @Override
    public void deletePlayer(Player player) {
    }

}